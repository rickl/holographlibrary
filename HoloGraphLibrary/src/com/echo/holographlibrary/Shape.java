package com.echo.holographlibrary;

import android.graphics.Path;
import android.graphics.Region;

/**
 * Created by rick on 1/28/14.
 */
public class Shape {
    private Path path;
    private Region region;

    public Path getPath() {
        return path;
    }

    public void setPath(Path path) {
        if (this.path == null) {
            this.path = new Path();
        }
        this.path.set(path);
    }

    public Region getRegion() {
        return region;
    }

    public void setRegion(int left, int top, int right, int bottom) {
        if (region == null) {
            region = new Region();
        }
        region.set(left, top, right, bottom);
    }

    public void setRegion(Region region) {
        if (this.region == null) {
            this.region = new Region();
        }
        this.region.set(region);
    }
}
